import React from "react";
import ReactDOM from "react-dom";
import "./css/base.scss";
import { Route, BrowserRouter as Router, Switch } from "react-router-dom";
import TrackPage from "./page/track/track-page";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHandPeace } from "@fortawesome/free-solid-svg-icons";
import ErrorPage from "./page/error/error-page";
import ParametersPage from "./page/parameters-page";
import { LANDING, TRACK, PARAMETERS } from "./constants/routes";
import TrackListPage from "./page/track-list/track-list-page";
import { Link } from "react-router-dom";

const routing = (
  <Router>
    <nav>
      <Link className="menu" to="/">
        <FontAwesomeIcon className="logo-icon" icon={faHandPeace} />
        <p className="app-name">Reach it</p>
      </Link>
    </nav>
    <div className="single-page-app">
      <Switch>
        <Route exact path={LANDING} component={TrackListPage} />
        <Route path={TRACK} component={TrackPage} />
        <Route path={PARAMETERS} component={ParametersPage} />
        <Route component={ErrorPage} />
      </Switch>
    </div>
  </Router>
);
ReactDOM.render(routing, document.getElementById("root"));
