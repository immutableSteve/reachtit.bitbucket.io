import { FirebaseClient } from "./firebase-client";

export class TrackListService {
  constructor() {
    this.client = new FirebaseClient();
  }

  saveTrackListOf(email, arrayOfTracks) {
    this.client.delete("tracklist", email);
    this.client.add("tracklist", email, {
      tracking: arrayOfTracks
    });
  }

  getTrackListOf(email, done) {
    return this.client.get("tracklist", email, done);
  }
}
