import { FirebaseClient } from "./firebase-client";

export class TrackService {
  constructor() {
    this.client = new FirebaseClient();
  }

  track(email, tracking, data) {
    const enrichedData = this.enrichData(data)
    this.getTracks(
      email,
      val => this.trackDone(val, tracking, enrichedData, email),
      err => alert(err)
    );
  }

  getTracks(email, done, error) {
    this.client.get("users", email, done, error);
  }

  enrichData(data) {
    return {
      value: data,
      date: this.getTimestamp()
    }
  }

  trackDone(val, tracking, data, email) {
    let trackToUpdate = val.data().tracks.find(val => val.title === tracking);
    if (!!trackToUpdate) {
      this.client.removeTracksArray("users", email, trackToUpdate);
      trackToUpdate.trackEvents.push(data);
    } else {
      trackToUpdate = { title: tracking, status: true, trackEvents: [data] };
    }
    this.client.updateTracksArray(val.ref, trackToUpdate);
  }

  filter() {
    return val => val;
  }

  getTimestamp() {
    return new Date(
      new Date().getFullYear(),
      new Date().getMonth(),
      new Date().getDate()
    ).getTime();
  }
}
