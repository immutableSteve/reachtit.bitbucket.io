import { FirebaseClient } from "./firebase-client";


export class UserService {
  constructor() {
    this.client = new FirebaseClient();
  }

  addUser(firstName, lastName, email) {
    this.client.add("users", email, {
      firstName: firstName,
      lastName: lastName,
      email: email,
      tracks: []
    });
  }

  getUser(email, done) {
    return this.client.get("users", email).then((val) => done(val.data()));
  }
}
  