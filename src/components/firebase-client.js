const firebase = require("firebase");
// Required for side-effects
require("firebase/firestore");

firebase.initializeApp({
  apiKey: "AIzaSyCVU-nbF8tADbMFtb2VtnGiGdg6UIIK7iU",
  authDomain: "squirrelz-reachit.firebaseapp.com",
  projectId: "squirrelz-reachit"
});

const db = firebase.firestore();

export class FirebaseClient {
  add(document, id, object) {
    db.collection(document)
      .doc(id)
      .set(object)
      .then(function(docRef) {
        if (docRef) console.log("Document written with ID: ", docRef.id);
      })
      .catch(function(error) {
        console.error("Error adding document: ", error);
      });
  }

  get(document, id, done, error) {
    db.collection(document)
      .doc(id)
      .get()
      .then(function(doc) {
        if (doc.exists) {
          return done(doc);
        } else {
          throw new Error(`Unknown object in ${document} with id ${id}`);
        }
      })
      .catch(function(err) {
        error(err);
      });
  }

  delete(document, id) {
    db.collection(document)
      .doc(id)
      .delete()
      .then(function() {
        console.log("Document successfully deleted!");
      })
      .catch(function(error) {
        console.error("Error removing document: ", error);
      });
  }

  updateTracksArray(ref, object) {
    ref.update({
      tracks: firebase.firestore.FieldValue.arrayUnion(object)
    });
  }

  removeTracksArray(document, id, object) {
    var ref = db.collection(document).doc(id);
    console.log(ref);
    ref.update({
      tracks: firebase.firestore.FieldValue.arrayRemove(object)
    });
  }
}
