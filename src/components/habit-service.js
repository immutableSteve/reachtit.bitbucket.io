import { FirebaseClient } from "./firebase-client";

export class HabitService {
  constructor() {
    this.client = new FirebaseClient();
  }

  getPredefinedHabits() {
    return [
      {name: "philosophy", explanation:"Did you find opportunities to be in alignment with your philosophy today?"}, 
      {name: "character", explanation:"Did you exemplify one of your character strengths today?"}, 
      {name: "vision", explanation:"Did you do something today toward your vision?"}, 
      {name: "optimism", explanation:"Did you notice three good things today?"}, 
      {name: "mindfulness", explanation:"How many minutes did you train mindfulness today?"}, 
      {name: "control", explanation:"Did you do your morning mindset routine today?"}, 
      {name: "eat well", explanation:"Did you eat only homemade food today?"}, 
      {name: "hydrate well", explanation:"Did you drink at least 1.2L of water today?"}, 
      {name: "sleep well", explanation:"Did you slept at least 7h this night?"}];
  }

  getHabit(name) {
    return this.getPredefinedHabits().find(val => val.name === name)
  }

}
  