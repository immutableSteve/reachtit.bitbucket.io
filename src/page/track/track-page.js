import React from "react";
import Track from "./track.js";
import { HabitService } from "../../components/habit-service.js";

class TrackPage extends React.Component {
  
  constructor(props) {
    super(props);
    this.habitService = new HabitService();
    this.habit = this.habitService.getHabit(this.props.match.params.id);
  }

  render() {
    return (
      <div className="page">
        <Track
          title={this.habit.name}
          text={this.habit.explanation}
          noAction={() => alert("no")}
          yesAction={() => alert("yes")}
        ></Track>
      </div>
    );
  }
}
export default TrackPage;
