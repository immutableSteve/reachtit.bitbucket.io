import React from "react";
import "./track.scss";
import { TrackService } from "../../components/track-service";
import { UserService } from "../../components/users-service";

class Track extends React.Component {
  constructor(props) {
    super(props);
    this.text = props.text;
    this.title = props.title;
    if (props.subtext) {
      this.subtext = `"${props.subtext}"`;
    } else {
      this.subtext = "";
    }
    this.trackService = new TrackService();
    this.userService = new UserService();
  }

  render() {
    return (
      <div className="track">
        <h3 className="title">{this.title}</h3>
        <div className="text">{this.text}</div>
        <div className="subtext">{this.subtext}</div>
        <div className="center yes-no">
          <div className="no option" onClick={() => this.trackService.track('test@test.be', this.title,"no")}>
            No
          </div>
          <div className="yes option" onClick={() => this.trackService.track('test@test.be', this.title,"yes")}>
            Yes
          </div>
        </div>
      </div>
    );
  }
}

export default Track;
