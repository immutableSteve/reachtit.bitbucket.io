import React from "react";
import "./track-list-page.scss";
import { Link } from "react-router-dom";
import { HabitService } from "../../components/habit-service";

class TrackListPage extends React.Component {
  constructor(props) {
    super(props);
    this.habitService = new HabitService();
  }

  render() {
    return (
      <div className="container">
        <h1 className="track-list-title">Track your habbits</h1>
        {this.habitService.getPredefinedHabits().map(value => {
          return (
            <div className="track-check" key={`${value.name}-check`}>
              <Link to={`/track/${value.name}`}>{value.name}</Link>
            </div>
          );
        })}
      </div>
    );
  }
}

export default TrackListPage;
