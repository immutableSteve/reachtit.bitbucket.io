import React from "react";
import { TrackListService } from "../components/track-list-service.js";

class ParametersPage extends React.Component {
  constructor() {
    super();
    this.trackListService = new TrackListService();
    this.selectedCheckboxes = new Set();
  }

  toggleCheckbox = label => {
      console.log(label)
    if (this.selectedCheckboxes.has(label)) {
      this.selectedCheckboxes.delete(label);
    } else {
      this.selectedCheckboxes.add(label);
    }
  };

  render() {
    return (
      <div>
          {this.trackListService.getTrackList().map(value => {
            return (
                <div>
                <input
                  type="checkbox"
                  onChange={() => this.toggleCheckbox({value})}
                  value={value}
                  id={value}
                />
                {value}
                </div>
            )
          })}
        <input
          type="button"
          value="save"
          onClick={() => this.trackListService.saveTrackListOf('steve email', [...this.selectedCheckboxes])}
        />
      </div>
    );
  }
}
export default ParametersPage;
