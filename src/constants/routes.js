export const LANDING = '/';
export const LOG_IN = '/login';
export const HOME = '/home';
export const TRACK = '/track/:id';
export const PARAMETERS = '/parameters';